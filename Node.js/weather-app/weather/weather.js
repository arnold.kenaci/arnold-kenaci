const request = require('request');

var getWeather = (lat, lng, callback) => {
  request({
    url: `https://api.forecast.io/forecast/4ac6be34688438787358a95123e5e66b/${lat},${lng}`,
    json: true
  }, (error, response, body) => {
    if (error) {
      callback('E pamundur lidhja me forecast.io');
    } else if (response.statusCode === 400) {
      callback('E pamundur te percaktohet temperatura.');
    } else if (response.statusCode === 200) {
      callback(undefined, {
        temperature: body.currently.temperature,
        apparentTemperature: body.currently.apparentTemperature
      });
    }
  });
};

module.exports.getWeather = getWeather;
