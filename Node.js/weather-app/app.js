const yargs = require('yargs');

const geocode = require('./geocode/geocode');
const weather = require('./weather/weather');

const argv = yargs
  .options({
    a: {
      demand: true,
      alias: 'address',
      describe: 'Adresa qe kerkojme te dime temperaturen?(Vendoset pas argumentit -a ne thonjeza)',
      string: true
    }
  })
  .help()
  .alias('help', 'h')
  .argv;

geocode.geocodeAddress(argv.address, (errorMessage, results) => {
  if (errorMessage) {
    console.log(errorMessage);
  } else {
    console.log(results.address);
    weather.getWeather(results.latitude, results.longitude, (errorMessage, weatherResults) => {
      if (errorMessage) {
        console.log(errorMessage);
      } else {
        console.log(`Temperatura eshte ${Math.round((weatherResults.temperature -32) * 5/9)}. Temperatura real-feel eshte ${Math.round((weatherResults.apparentTemperature - 32) * 5/9)}.`);
      }
    });
  }
});
